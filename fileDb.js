const fs = require("fs");
const path = "./messages";

module.exports = {
  save(message, datetime) {
    fs.writeFileSync(path + "/" + datetime + ".txt", JSON.stringify({ ...message, datetime: datetime }));
  },
};
