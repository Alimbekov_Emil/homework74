const express = require("express");
const fileDb = require("../fileDb");
const fs = require("fs");

const router = express.Router();

router.get("/", (req, res) => {
  fs.readdir("./messages", (err, files) => {
    const messages = [];
    files.forEach((file) => {
      messages.push(JSON.parse(fs.readFileSync("./messages" + "/" + file, "utf-8")));
    });
    if (messages.length > 5) {
      return res.send(messages.slice(messages.length - 5));
    }
    return res.send(messages);
  });
});

router.post("/", (req, res) => {
  const datetime = new Date().toISOString();
  fileDb.save(req.body, datetime);
  res.send({ ...req.body, datetime });
});

module.exports = router;
